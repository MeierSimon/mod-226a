package edu.ab05;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void add() {
        int expected = 17;
        int result = calculator.calc(10, 7, 1);

        // successful --> addition by positive number
        assertEquals(expected, result);

        // successful --> addition by negative number
        assertEquals(10, calculator.calc(15, -5, 1));

        // successful --> addition by zero
        assertEquals(20, calculator.calc(20, 0, 1));


    }

    @Test
    public void subtract() {
        int expected = 20;
        int result = calculator.calc(35, 15, 2);

        // successful --> subtraction by positive number
        assertEquals(expected, result);

        // successful --> subtraction by negative number
        assertEquals(30, calculator.calc(25, -5, 2));

        // successful --> subtraction by zero
        assertEquals(10, calculator.calc(10, 0, 2));


    }

    @Test
    public void multiply() {
        int expected = 12;
        int result = calculator.calc(4, 3, 3);

        // successful --> multiplication by positive number
        assertEquals(expected, result);

        // successful --> multiplication by negative number
        assertEquals(-20, calculator.calc(5, -4, 3));

        // successful --> multiplication by zero
        assertEquals(0, calculator.calc(26, 0, 3));


    }

    @Test
    public void divide() {
        int expected = 7;
        int result = calculator.calc(21, 3, 4);

        // successful --> division by positive number
        assertEquals(expected, result);

        // successful --> division by negative number
        assertEquals(-7, calculator.calc(21, -3, 4));

    }

    @Test
    public void modulo() {
        int expected = 0;
        int result = calculator.calc(15, 5, 5);

        // successful --> modulo by positive and odd number
        assertEquals(expected, result);

        // successful --> modulo by negative number
        assertEquals(expected, calculator.calc(15, -5, 5));


    }


}

