package edu.ab05;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BerechnenVonPiTest {
    BerechnenVonPi berechnenVonPi;

    @BeforeEach
    public void setUp(){
        berechnenVonPi = new BerechnenVonPi();
    }

    @Test
    void calculate() {
        assertEquals(Math.PI, berechnenVonPi.calculate(0,1,1),0.01);
    }
}