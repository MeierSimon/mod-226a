package edu.ab01.oop.logik;

import org.junit.jupiter.api.Test;

public class CodeGeneratorTest {
    @Test
    public void testGenerateCode(){
        CodeGenerator cg = new CodeGenerator();
        char[] secretCode = cg.generateCode(8, new char[]{'a', 'b'});
        System.out.println(secretCode);
    }
    @Test
    public void testGenerateCode2(){
        CodeGenerator cg = new CodeGenerator();
        char[] secretCode = cg.generateCode(4, new char[]{'a', 'b', 'c', 'd'});
        System.out.println(secretCode);
    }
    @Test
    public void testGenerateCode3(){
        CodeGenerator cg = new CodeGenerator();
        char[] secretCode = cg.generateCode(4, new char[]{'a', 'b', 'c', 'd', 'e', 'f'});
        System.out.println(secretCode);
    }
    @Test
    public void testGenerateCode4(){

        CodeGenerator cg2 = new CodeGenerator();
        char[] secretCode = cg2.generateCode(4, new char[]{'a', 'b', 'c', 'd', 'e', 'f'});
        System.out.println(secretCode);
    }
}

