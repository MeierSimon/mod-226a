package edu.ab01;

public class HelloWorld {

    /**
     * Erstes Programm, "Hello World" auf der Konsole ausgeben
     */
    public static void main(String[] args) {
        System.out.println("Hello World");

    }
}
