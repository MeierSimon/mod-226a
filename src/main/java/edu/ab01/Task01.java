package edu.ab01;

public class Task01 {

    /**
     * Erstes Programm
     * Gibt "Dies ist mein erstes Java-Programm." auf der Konsole aus
     */
    public static void main(String[] args) {

        System.out.println("Dies ist mein erstes Java-Programm.");
    }
}
