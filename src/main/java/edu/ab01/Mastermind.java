package edu.ab01;

import java.util.Arrays;
import java.util.Scanner;

public class Mastermind {

    public static final char[] colors = new char[]{'r', 'g', 'b', 'y', 'w', 's'};
    public static char[] userCode = new char[4];
    public static char[] secretCode = new char[4];
    public static int tries = 10;


    public static void main(String[] args) {

        play();

        while (tries > 0 || compareInputAndSecretCode()) {
            String input;
            do {
                System.out.println("Geben Sie einen Versuchscode mit vier Buchstaben aus der Menge " + Arrays.toString(colors) + " ein:");
                input = readUserInput();
                compareInputAndSecretCode();
            } while (checkUserInput(input));
        }
    }

    public static void play() {

        generateSecretCode();

    }

    /**
     * Liest den Input des Users ein
     * @return userCode
     */
    public static String readUserInput() {

        Scanner scn = new Scanner(System.in);
        String userInput = scn.nextLine();
        for (int x = 0; x < 4; x++) {
            userCode[x] = userInput.charAt(x);
        }return Arrays.toString(userCode);

    }

    /**
     * generiert den Secretcode
     */
    public static void generateSecretCode() {

        // SecretCode generieren:
        for (int x = 0; x < 4; x++) {
            secretCode[x] = colors[((int) (Math.random() * 6))];
        }

    }

    /**
     * Vergleicht den Userinput mit dem Secretcode
     * @return win
     */
    public static boolean compareInputAndSecretCode() {
        boolean win = false;

        if (Arrays.equals(userCode, secretCode)) {
            System.out.println("Du gewinnst!");
            win = true;
        } else if (tries > 0){
            int correctColorAndPlace = 0;
            int correctColorWrongPlace = 0;


            for (int index = 0; index < secretCode.length; index++) {
                if (userCode[index] == secretCode[index]) {
                    correctColorAndPlace++;
                }
            }


            for (char c : userCode) {
                for (int index = 0; index < secretCode.length; index++) {
                    if ((c == secretCode[index]) && (userCode[index] != secretCode[index])) {
                        correctColorWrongPlace++;
                    }
                }
            }

            // Ausgeben wie viele Stifte korrekt sind und wie viele Farben stimmen aber nicht an korrekter Porition
            System.out.println("Anzahl Stifte mit korrekter Farbe & Platz: " + correctColorAndPlace);
            System.out.println("Anzahl Stifte mit korrekter Farbe, aber an falschem Platz: " + correctColorWrongPlace);

            tries--;
            System.out.println("Versuchs nochmal. Übrige Versuche: " + tries);
        } else if (tries == 0) {
            System.out.println("Spiel beendet. Geheimcode war " + String.valueOf(secretCode));
        }
        return win;
    }

    /**
     * Überprüft ob der Usercode mit den gültigen Buchstaben übereinstimmt
     * @param input Eingabe des Users
     * @return gültige Buchstaben
     */
    public static boolean checkUserInput(String input) {
        int validColors = 0;

        if (input.length() > secretCode.length || input.length() < secretCode.length) {
            return false;
        } else {
          userCode = new char[input.length()];
          for (int i = 0; i < input.length(); i++) {
              userCode[i] = input.charAt(i);
          }
            // compare user's colors with predefined colors
            for (char c : userCode) {
                for (int index = 0; index < colors.length; index++) {
                    if (c == colors[index]) {
                        validColors++;
                    }
                }
            }
            return validColors == 4;
        }
    }


}
