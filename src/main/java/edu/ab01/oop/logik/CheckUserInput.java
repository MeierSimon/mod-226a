package edu.ab01.oop.logik;

public class CheckUserInput {

    /** Überprüft ob die Eingabe des Users den Regeln entspricht
     * @return boolean isValid */

    public static boolean validateInput(char[] colors, int codeSize, char[] userCode){

        if (userCode.length < codeSize || userCode.length > codeSize){
            return false;
        }
        boolean isValid = false;
        for (int x = 0; x < codeSize; x++){
            char currentLetter = userCode[x];
            for (int y = 0; y < colors.length; y++){
                char validLetter = colors[y];
                if (currentLetter == validLetter){
                    isValid = true;
                    break;
                }isValid = false;
            }
        }return isValid;

    }
}
