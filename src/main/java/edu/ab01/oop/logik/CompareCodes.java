package edu.ab01.oop.logik;

import edu.ab01.oop.model.MoveResult;

import java.util.Arrays;

public class CompareCodes {

    /** Vergleich des userCode mit dem secretCode
     * @return currentResult */

    public static MoveResult compareInputAndSecretCode(char[] userCode, char[] secretCode, int tries){

        MoveResult currentResult;
        currentResult = new MoveResult();
        currentResult.setWin(false);
        currentResult.setCorrectColorAndPlace(0);
        currentResult.setCorrectColorWrongPlace(0);

        if (Arrays.equals(userCode, secretCode)) {
            currentResult.setWin(true);
        } else if (tries > 0){


            for (int index = 0; index < secretCode.length; index++) {
                if (userCode[index] == secretCode[index]) {
                    currentResult.setCorrectColorAndPlace(currentResult.getCorrectColorAndPlace()+1);
                }
            }


            for (char c : userCode) {
                for (int index = 0; index < secretCode.length; index++) {
                    if ((c == secretCode[index]) && (userCode[index] != secretCode[index])) {
                        currentResult.setCorrectColorWrongPlace(currentResult.getCorrectColorWrongPlace()+1);
                    }
                }
            }

            // Ausgeben wie viele Stifte korrekt sind und wie viele Farben stimmen aber nicht an korrekter Porition



        }
        return currentResult;
    }
}
