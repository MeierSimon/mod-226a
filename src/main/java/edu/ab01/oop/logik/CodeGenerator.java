package edu.ab01.oop.logik;

public class CodeGenerator {

    /** Generiert eine secreCode
     * @return secretCode */

    public char[] generateCode(int codeSize, char[] colors) {

        char[] secretCode = new char[codeSize];

        // SecretCode generieren:
        for (int x = 0; x < codeSize; x++) {
            double randomNumberDouble = Math.random() * colors.length;
            int randomIndex = (int) (randomNumberDouble);
            char randomChar = colors[randomIndex];
            secretCode[x] = randomChar;
        }
        return secretCode;
    }
}
