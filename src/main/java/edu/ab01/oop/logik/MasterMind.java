package edu.ab01.oop.logik;

import edu.ab01.oop.model.GameSetup;
import edu.ab01.oop.model.MoveResult;

public class MasterMind {

    private final GameSetup gameSetup;
    private char[] userCode;

    /** Variabeln welche für das gesamte spiel benötigt werden, werden deklariert */

    public MasterMind() {

        gameSetup = new GameSetup();
        gameSetup.setCodeSize(4);
        char[] myColors = new char[]{'r', 'g', 'b', 'y', 'w', 's'};
        gameSetup.setColors(myColors);
        gameSetup.setTries(10);
    }

    /** Einstieg in das Spiel.
     * Ausgaben zu aktuellem spielstand
     * Ausgaben zu gewonnen oder verloren */

    public void play() {

        CodeGenerator cg1 = new CodeGenerator();
        gameSetup.setSecretCode(cg1.generateCode(gameSetup.getCodeSize(), gameSetup.getColors()));
        int Tries = gameSetup.getTries();
        MoveResult currentResult;

        do {


            userCode = UserInput.readUserInput(gameSetup.getCodeSize(), gameSetup.getColors());
            currentResult = CompareCodes.compareInputAndSecretCode(userCode, gameSetup.getSecretCode(), gameSetup.getTries());
            if (!currentResult.isWin()) {
                Tries--;
                System.out.println("Anzahl Stifte mit korrekter Farbe & Platz: " + currentResult.getCorrectColorAndPlace());
                System.out.println("Anzahl Stifte mit korrekter Farbe, aber an falschem Platz: " + currentResult.getCorrectColorWrongPlace());
                System.out.println("Versuchs nochmal. Übrige Versuche: " + Tries);
            }


        } while (Tries > 0 && !currentResult.isWin());

        if (Tries == 0) {
            System.out.println("Spiel beendet. Geheimcode war " + String.valueOf(gameSetup.getSecretCode()));
        } else {
            System.out.println("Du gewinnst!");
            System.out.println("Geheimcode war " + String.valueOf(gameSetup.getSecretCode()));
        }


    }
}
