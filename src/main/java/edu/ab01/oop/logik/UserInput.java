package edu.ab01.oop.logik;

import java.util.Arrays;
import java.util.Scanner;

public class UserInput {

    /** Einlesen des UserCodes
     * if isValid == True
     * @return char[] userCode
     * else Falsche Eingabe*/

    public static char[] readUserInput(int codeSize, char[] colors) {

        char[] userCode;

        System.out.println("Geben Sie einen Versuchscode mit vier Buchstaben aus der Menge" + Arrays.toString(colors) + " ein:");

        Scanner scn = new Scanner(System.in);
        userCode = scn.nextLine().toCharArray();
        if (CheckUserInput.validateInput(colors, codeSize, userCode)) {
            return userCode;
        } else {
            System.out.println("Falsche Eingabe!!!");
            return UserInput.readUserInput(codeSize, colors);
        }
    }
}
