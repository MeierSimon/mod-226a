package edu.ab01.oop.model;

public class MoveResult {

    /** Beinhaltet Variabeln zum aktuellen Spielstand */

    int correctColorAndPlace;
    int correctColorWrongPlace;
    boolean win;

    public int getCorrectColorAndPlace() {
        return correctColorAndPlace;
    }

    public int getCorrectColorWrongPlace() {
        return correctColorWrongPlace;
    }

    public boolean isWin() {
        return win;
    }

    public void setCorrectColorAndPlace(int correctColorAndPlace) {
        this.correctColorAndPlace = correctColorAndPlace;
    }

    public void setCorrectColorWrongPlace(int correctColorWrongPlace) {
        this.correctColorWrongPlace = correctColorWrongPlace;
    }

    public void setWin(boolean win) {
        this.win = win;
    }


}
