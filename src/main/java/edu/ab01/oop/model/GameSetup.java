package edu.ab01.oop.model;

public class GameSetup {

    /** GameSetup beinhaltet Variabeln, welche während des gesamten Spiels benötigt werden */


    private char[] colors;
    private char[] secretCode;
    private int codeSize;
    private int tries;

    public void setCodeSize(int codeSize) {
        this.codeSize = codeSize;
    }

    public int getCodeSize() {
        return this.codeSize;
    }

    public char[] getColors() {
        return colors;
    }

    public void setColors(char[] colors) {
        this.colors = colors;
    }

    public char[] getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(char[] secretCode) {
        this.secretCode = secretCode;
    }

    public int getTries() {
        return tries;
    }

    public void setTries(int tries) {
        this.tries = tries;
    }
}
