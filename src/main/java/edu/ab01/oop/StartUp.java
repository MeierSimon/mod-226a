package edu.ab01.oop;

import edu.ab01.oop.logik.MasterMind;

/** Klasse StartUp um das Spiel Mastermind zu starten */

public class StartUp {

    public static void main(String[] args) {

        new MasterMind().play();

    }
}
