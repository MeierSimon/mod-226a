package edu.ab03;

public class Task01 {

    /**
     * Dieses Programm gibt ein Dreieck aus Sternen auf der Konsole aus
     */

    public static char stern = '*';

    public static void main (String[] args) {

        int zeilen = 1;
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < zeilen; x++) {
                System.out.print(stern);
            }
            zeilen++;
            System.out.println();
        }

    }
}
