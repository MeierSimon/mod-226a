package edu.ab03;

import java.util.Random;

public class TaskXO {


    /**
     * Array mit 0 initialisieren
     */

    private static char[] initArray(int numberOfValues) {
        char[] returnValue = new char[numberOfValues];
        for (int index = 0; index < numberOfValues; index++) {
            returnValue[index] = 'O';
        }
        return returnValue;
    }


    public static void main (String[] args){

        // Initialisieren Anzahl Linien
        // Initialisieren Anzahl Zeichen pro Linie
        final int numberOfLines = 10;
        final int numberOfValues = 10;


        // Zufallstahlengenerator
        Random randomGenerator = new Random();

        for (int line = 1; line <= numberOfLines; line++) {
            int randomNumber = randomGenerator.nextInt(4) + 1;
            int numberOfZeroes = Math.floorDiv(numberOfValues - 1, randomNumber);

            char[] output = initArray((numberOfValues));
            for (int position = 0; position < numberOfValues; position +=  numberOfZeroes + 1) {
                output[position] = 'X';
            }

            System.out.println("Zeile " + line + ": Zufallszahl: " + randomNumber +
                    " " + String.valueOf(output));
        }
    }


}
