package edu.ab04;

import java.util.Arrays;
import java.util.Random;

public class Task01 {

    /**
     * Dieses Programm erstellt ein Array mit 30 Zufallszahlen.
     * Anschliessend wird das Array kopiert, sortiert und mit dem alten Array verglichen.
     * Beide Arrays werden zum Schluss auf der Konsole ausgegeben.
     */

    public static void main (String[] args) {

        final int numberOfValues = 30; //Länge des Arrays soll 30 sein
        int[] unsortedValues = new int[30]; //Initialisieren eines Arrays mit 30 Werten
        int[] sortedValues = new int[30]; //Initialisieren des zweiten Arrays mit 30 Werten



        //Zufallswerte erzeugen
        for (int x = 0; x < numberOfValues; x++) {
            unsortedValues[x] = ((int)(Math.random()*50)+1);
        }
        // Array kopieren
        System.arraycopy(unsortedValues, 0, sortedValues, 0, 30);
        Arrays.sort(sortedValues);
        // Beide Arrays ausgeben
        System.out.println(Arrays.toString(unsortedValues));
        System.out.println(Arrays.toString(sortedValues));

        // Zähler deklarieren
        int counter = 0;

        // Arrays vergleichen
        for (int x = 0; x < numberOfValues; x++){
            if (unsortedValues[x] > sortedValues[x]) counter++;
        }
        System.out.println("An " + counter + " Positionen waren im unsortierten Array grössere Zahlen.");
    }


}
