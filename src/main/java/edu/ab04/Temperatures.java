package edu.ab04;

import java.util.Arrays;
import java.util.Random;

public class Temperatures {


        public static final int days = 6, places = 3, times = 4;
        public static int[][][] temperatures = new int[days][places][times];
        public static int minValue = 0;

        public static void main(String[] args) {

            Temperatures temp = new Temperatures();

            temp.generateRandomTemp();

            System.out.println(Arrays.deepToString(temperatures).replace("[[[", "(").replace("]]]", ")").replace("]], ", ")\n").replace("[[", "(").replace("[", "(").replace("],", ")"));

            minValue = temp.findMinValue();
            System.out.println("Minimum der Zahlen ist: " + minValue + ", an Position:");

            temp.findMinValuePosition();
        }

        /**
         * generiert zufallswerte zwischen 20 und 30
         * und befüllt das 3D Array
         */
        public void generateRandomTemp() {
            Random random = new Random();

            for (int rows = 0; rows < days; rows++) {
                for (int cols = 0; cols < places; cols++) {
                    for (int value = 0; value < times; value++) {
                        temperatures[rows][cols][value] = random.nextInt(10) + 20;
                    }
                }
            }
        }

        /**
         * loop über das Array
         * Gibt den kleinsten Wert im Array zurück
         */
        public int findMinValue() {
            int minValue = temperatures[0][0][0];

            for (int i = 0; i < temperatures.length; i++) {
                for (int x = 0; x < temperatures[i].length; x++) {
                    for (int y = 0; y < temperatures[i][x].length; y++) {
                        if (temperatures[i][x][y] < minValue) {
                            minValue = temperatures[i][x][y];
                        }
                    }
                }
            }
            return minValue;
        }

        /**
         * suche nach der Position des kleinsten Wertes im Array
         */
        public void findMinValuePosition() {
            int index1 = 0;
            int index2 = 0;
            int index3 = 0;

            for (int i = 0; i < temperatures.length; i++) {
                for (int j = 0; j < temperatures[i].length; j++) {
                    for (int k = 0; k < temperatures[i][j].length; k++) {
                        if (temperatures[i][j][k] == minValue) {
                            index1 = i;
                            index2 = j;
                            index3 = k;
                            System.out.println("Zeile " + index1 + ", Gruppe " + index2 + ", Element " + index3);
                        }
                    }
                }
            }
        }
    }


