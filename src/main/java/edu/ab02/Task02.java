package edu.ab02;

public class Task02 {



    public static void main(String[] args){

        /**
         * Variablen deklarieren
         */
        int numberOfStudents = 10;
        boolean lightSwitchState = false;
        double accountBalance = 15123.45;
        char chessLine = 'B';

        /**
         * Variablen ändern
         */
        numberOfStudents = 12;
        lightSwitchState = true;
        accountBalance = 22652.35;
        chessLine = 'D';

        /**
         * Variablen ausgeben:
         */
        System.out.println(numberOfStudents);
        System.out.println(lightSwitchState);
        System.out.println(accountBalance);
        System.out.println(chessLine);
        


    }
}
