package edu.ab05;

public class Calculator {

    /**
     * Rechner führt eine Berechnung von zwei Parameter aus, basierend auf Parameter drei
     * @param operand1 numeric value 1
     * @param operand2 numeric value 2
     * @param operator 1 = addition, 2 = subtraction, 3 = multiplication, 4 = division, 5 = modulo
     * @return Gibt das Resultat von operand1 und operand2 basierend auf operantor zurück
     */


    public static void main(String[] args) {

        for (int i = 1; i < 7; i++){
            int xy = (new Calculator()).calc(5,1,i);
            System.out.println(xy);
        }

    }

    public int calc(int operand1, int operand2, int operator){

        return switch (operator){
            case 1 -> operand1 + operand2;
            case 2 -> operand1 - operand2;
            case 3 -> operand1 * operand2;
            case 4 -> operand1 / operand2;
            case 5 -> operand1 % operand2;
            default -> throw new IllegalStateException("Unexcpected value: " + operator);
        };
    }
}
